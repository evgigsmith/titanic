import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()    
    df['category'] = df['Name'].str.extract(r'(Mr.|Mrs.|Miss.)')
    summary = df.groupby('category')['Age'].agg(Missing=lambda x: x.isnull().sum(), Median='median')
    result = list(summary.reset_index().itertuples(index=False, name=None))
    return result


